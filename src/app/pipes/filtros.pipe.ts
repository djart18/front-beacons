import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtros'
})
export class FiltrosPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    const result = [];

    for (const user of value) {
      if (user.name.toLowerCase().indexOf(arg.toLowerCase()) > -1) {
        result.push(user);
      }
    }

    return result;

  }

}
