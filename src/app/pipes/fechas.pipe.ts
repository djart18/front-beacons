import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fechas'
})
export class FechasPipe implements PipeTransform {

  transform(value: any, arg: any): any {

    const resp = [];

    for ( const clock of value ) {
      if ( clock.clock.toLowerCase().indexOf(arg.toLowerCase()) > -1 ) {
        resp.push(clock);
      }
    }

    return resp;
  }

}
