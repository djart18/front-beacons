import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuarios } from '../interfaces/interface';
import { Activities } from '../interfaces/interfaceActivities';
import { Checks } from '../interfaces/interfaceChecks';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get<Usuarios[]>('http://127.0.0.1:8000/api/Users');
  }

  getActivities(id: number) {
    return this.http.get<Activities[]>(`http://127.0.0.1:8000/${id}/${name}`);
  }

  savedata(data: any) {
    const url = 'http://127.0.0.1:8000/api/Users';
    return this.http.post(url, data, {
      headers: new HttpHeaders({ 'content-type': 'application/json' })
    });
  }

  getChecks() {
    return this.http.get<Checks[]>(`http://127.0.0.1:8000/api/checktest`);
  }q

}
