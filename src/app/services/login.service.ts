import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, BehaviorSubject } from 'rxjs';
import Swal from 'sweetalert2'
// import swal from 'sweetalert';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  protected = new BehaviorSubject(true);
  errorData: {};
  save: any;

  constructor(private http: HttpClient) { }

  login(useremail: string, userpassword: string ) {
    return this.http.post('http://127.0.0.1:8000/api/login', {email: useremail, password: userpassword}).pipe(map(user => {
        console.log(user);
        if (user) {
          localStorage.setItem('currentUser', JSON.stringify(user));
          Swal.fire({
            type: 'success',
            title: 'Bienvenido a la Gestión de empleados',
          })
          // swal("Bienvenido a la Gestión de empleados", "", "success");
        }
      }),
      catchError(this.handleError)
    );
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      // swal("Error", "Usuario o contraseña incorrectos", "error");
      Swal.fire({
        type: 'error',
        title: 'Error',
        text: 'Usuario o contraseña incorrectos',
      })
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    this.errorData = {
      errorTitle: 'Oops! Request for document failed',
      errorDesc: 'Something bad happened. Please try again later.'
    };
    return throwError(this.errorData);
  }

  isLoggedIn() {
    if (localStorage.getItem('currentUser')) {
      return true;
    } else {
      return false;
    }
  }

  save_storage(email: any, password: any) {
    this.save = {
      user_email: email,
      user_password: password
    };
    localStorage.setItem('currentUser', JSON.stringify(this.save));
    console.log('Datos que se guardo en el storage ', this.save);
  }

  noReturn(): boolean {
    console.log('entro en el metodo');

    if (localStorage.getItem('currentUser')) {
      this.protected.next(true);
     } else {
      this.protected.next(false);
     }
    console.log(this.protected.value);
    return this.protected.value;
 }
}
