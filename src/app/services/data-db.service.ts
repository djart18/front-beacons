import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Activities } from '../interfaces/interfaceActivities';

@Injectable({
  providedIn: 'root'
})

export class DataDbService {

  private api = 'http://127.0.0.1:8000/api/Activities';
  private apiUser = 'http://127.0.0.1:8000/api/Users';

  constructor(private http: HttpClient) { }

  createActivities(Uname, act: Activities, id) {
    act.d_name = Uname;
    act.estado = 'Inicializado';
    const path = 'http://127.0.0.1:8000/api/Activities';
    return this.http.post(path, act, id);
  }

  eliminarActividad(id: number) {
    const path = `${this.api}/${id}`;
    return this.http.delete(path);
  }

  eliminarUsuario(id: number) {
    const path = `${this.apiUser}/${id}`;
    return this.http.delete(path);
  }

  actualizarActividad(aid, act: Activities) {
    const path = `${this.api}/${aid}?`;
    return this.http.put(path, act);
  }
}
