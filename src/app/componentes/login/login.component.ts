import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error: {};
  submitted = false;
  loginError: string;
  loginForm: FormGroup;

  usuario = {
    advertencia: '',
    email: '',
    password: ''
  };

  constructor(private loginService: LoginService,
              private router: Router,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      useremail
      : ['', Validators.required],
      userpassword: ['', Validators.required]
    });
  }

  logIn() {
    this.submitted = true;
    this.loginService.login(this.usuario.email, this.usuario.password).subscribe(data => {
      console.log(data);

       if (this.loginService.isLoggedIn) {
          // const redirect = this.taskService.redirectUrl ? this.taskService.redirectUrl : '/home';
          console.log('entro en el storage');
          this.loginService.save_storage(this.usuario.email, this.usuario.password);
          this.router.navigate(['/inicio']);
        } else {
          this.loginError = 'Useremail or password is incorrect.';
        }
      },
      error => this.error = error
    );
  }

}
