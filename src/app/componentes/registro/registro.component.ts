import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  dataFromService: any = '';
  name: '';
  email: '';
  password: '';

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
  }

  savedata() {

    const data = {name: this.name,
                  email: this.email,
                  password: this.password
    };

    this.dataService.savedata(data).subscribe((resp) => {
      if (resp) {
        Swal.fire({
          type: 'success',
          title: 'Empleado registrado con éxito',
        })
      }
        this.dataFromService = JSON.stringify(resp);
        console.log(this.dataFromService);
        console.log('Datos:', this.name, this.email, this.password);
      });
  }
}
