import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Checks } from '../../interfaces/interfaceChecks';
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-marcacion-tarjeta',
  templateUrl: './marcacion-tarjeta.component.html',
  styleUrls: ['./marcacion-tarjeta.component.css']
})
export class MarcacionTarjetaComponent implements OnInit {
  
  ordenar: Checks[];
  checks: Checks[];
  buscar = '';
  casa: any;
  auto: any;
  fech: any;

  constructor(private dataService: DataService) { }
  ngOnInit() {
               
    return this.dataService.getChecks()     
      .subscribe( resp => {
        this.checks = resp;
        console.log(this.checks); 
        this.ajusteHora();   
        this._ordenar();
      });
  }

  buscarUsuario(event) {
    this.buscar = event.detail.value;
  }
  ajusteHora(){
    for(var i = 0; i<this.checks.length ; i++) {
      this.checks[i].clock = moment(this.checks[i].clock).format('LLL');
      console.log(this.checks[i].clock);
      this.checks[i].color = "#AED6F1";
      var auto=this.checks[i].clock.substring(16,this.checks[i].clock.length);
      this.checks[i].hora=auto;

      var casa=this.checks[i].clock.substring(0,16);  
      casa = moment(casa).format('DD/MM/YYYY');
      this.checks[i].clock=casa;
    }
  }
  _ordenar(){
    this.ordenar = this.checks.slice(0, this.checks.length);
    var longitud = this.checks.length;
    var i = 0;
    var cantidad = 1;
    while (i < this.checks.length) {
      this.ordenar[i] = this.checks [longitud - cantidad]
      i++;
      cantidad++;  
    }
  }
}