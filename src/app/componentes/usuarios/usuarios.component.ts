import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Usuarios,Activity } from '../../interfaces/interface';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpErrorResponse } from'@angular/common/http';
// import { ConsoleReporter } from 'jasmine';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  ordenar: Usuarios[];
  usuarios: Usuarios[];
  buscar = '';
  pageActual = 1;
  

  constructor(private dataService: DataService,
              private router: Router,
              private http: HttpClient) { }

  ngOnInit() {
    this.dataService.getUsers().subscribe( resp => {
      this.usuarios = resp;  
      this._ordenar();
      console.log('toods los empleados',this.usuarios)
    });
    }

   getActivities(id: number) {
    return this.http.get<Activity[]>(`http://192.168.137.1:8000/${id}`); 
  }

  buscarUsuario(event) {
    this.buscar = event.detail.value; 
  }

   onSelect(id: number, name: string) {
    this.router.navigate(['/detalle-usuario', id, name]);
  } 

  _ordenar(){
    this.ordenar = this.usuarios.slice(0, this.usuarios.length)
    var longitud = this.usuarios.length;
    var i = 0;
    var cantidad = 1;
    while (i < this.usuarios.length) {
      this.ordenar[i] = this.usuarios [longitud - cantidad]
      i++;
      cantidad++;
    }
    console.log(this.ordenar.length)
  }
}
