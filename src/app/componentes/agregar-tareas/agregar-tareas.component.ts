import { Component, OnInit } from '@angular/core';
import { DataDbService } from 'src/app/services/data-db.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Activities } from '../../interfaces/interfaceActivities';

@Component({
  selector: 'app-agregar-tareas',
  templateUrl: './agregar-tareas.component.html',
  styleUrls: ['./agregar-tareas.component.css']
})

export class AgregarTareasComponent implements OnInit {

  actividades: Activities = {
    name: '',
    estado: 'inicializado',
    description: '',
    avance: null,
    cliente: '',
    proyecto: '',
  };

  id: number;
  Uname: string;

  constructor(private datadbService: DataDbService,
              private route: ActivatedRoute,
              private router: Router) {
    this.Uname = route.snapshot.params.name;
    this.id = route.snapshot.params.id;
  }

  ngOnInit() { }

  sendActivities(id = this.id, name = this.Uname, act = this.actividades) {
    this.datadbService.createActivities(name, act, id)
      .subscribe(async data => {
        console.log('id', id, 'name', name, 'act', act, 'data', data);
        await this.router.navigate(['/detalle-usuario', id, name]);
      },
        error => {
          console.log('error', error);
        });
  }

}
