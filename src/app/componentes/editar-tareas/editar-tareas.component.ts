import { Component, OnInit } from '@angular/core';
import { DataDbService } from '../../services/data-db.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-editar-tareas',
  templateUrl: './editar-tareas.component.html',
  styleUrls: ['./editar-tareas.component.css']
})
export class EditarTareasComponent implements OnInit {

  id: number;
  aid: number;
  uname: string;

  actividades = {
    name: '',
    description: '',
    avance: null,
    cliente: '',
    proyecto: '',
  };

  constructor(private datadbService: DataDbService,
              private route: ActivatedRoute,
              private router: Router) {
    this.id = route.snapshot.params.id;
    this.aid = route.snapshot.params.aid;
    this.uname = route.snapshot.params.name;
    this.actividades.name = route.snapshot.params.aname
    this.actividades.cliente = route.snapshot.params.acliente
    this.actividades.proyecto = route.snapshot.params.aproyecto
    this.actividades.description = route.snapshot.params.adescription
    this.actividades.avance = route.snapshot.params.aavance
  }

  ngOnInit() {
    console.log(this.actividades.name, this.actividades.description, this.actividades.avance, this.actividades.cliente, this.actividades.proyecto);
  }

  editarActividad(aid: number, act = this.actividades) {
    this.datadbService.actualizarActividad(aid, act)
      .subscribe(async resp => {
        console.log(resp, aid);
        await this.router.navigate(['/detalle-usuario', this.id, this.uname]);
      });
  }

}
