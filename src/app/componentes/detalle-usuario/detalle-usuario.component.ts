import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { Activities } from '../../interfaces/interfaceActivities';
import { ActivatedRoute, Router } from '@angular/router';
import { DataDbService } from '../../services/data-db.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-detalle-usuario',
  templateUrl: './detalle-usuario.component.html',
  styleUrls: ['./detalle-usuario.component.css']
})  

export class DetalleUsuarioComponent implements OnInit {

  // Eliminar actividad
  public popoverTitle = 'Eliminar actividad';
  public popoverMessage = '¿Está seguro de eliminar esta actividad?';
  public cancelClicked = false;

  // Eliminar usuario
  public Titulo = 'Eliminar empleado';
  public Mensaje = '¿Está seguro de eliminar este empleado?';
  public cancel = false;

  ordenar: Activities[];
  actividad: Activities[];
  id: number;
  name: string;
  pageActual = 1;

  constructor(private dataService: DataService,
              private datadbService: DataDbService,
              private route: ActivatedRoute,
              private router: Router,
              private location: Location) {
    this.id = route.snapshot.params.id;
    this.name = route.snapshot.params.name;
  }

  ngOnInit() {
    this.dataService.getActivities(this.id).subscribe(resp => {
      this.actividad = resp;
      this._ordenar();
    });
  }

  _ordenar(){
    this.ordenar = this.actividad.slice(0, this.actividad.length)
    var longitud = this.actividad.length;
    var i = 0;
    var cantidad = 1;
    while (i < this.actividad.length) {
      this.ordenar[i] = this.actividad [longitud - cantidad]
      i++;
      cantidad++;
    }
  }

  agregarTareas(id: number, name: string) {
    this.router.navigate(['/agregar-tareas', id, name]);
  }

  eliminarActividades(id: number, uid = this.id, uname = this.name) {

    this.datadbService.eliminarActividad(id).subscribe( resp => {
      console.log('Resp', resp, 'Se eliminó la actividad con el id:', id );

      // Refrescar la página
      this.router.navigateByUrl('/detalle-usuario', { skipLocationChange: true }).then(() => {
        this.router.navigate([decodeURI(this.location.path())]);
      });

    });
  }

  editarActividad(id: number, aid: number, name: string, aname: string, acliente: string, aproyecto: string, adescription: string, aavance: string, aestado: string) {
    this.router.navigate(['/editar-tareas', id, aid, name, aname, acliente, aproyecto, adescription, aavance, aestado]);
  }

  eliminarUsuario(id: number) {
    this.datadbService.eliminarUsuario(id).subscribe(async resp => {
      console.log( 'Resp', resp, 'Se eliminó el usuario con el id:', id );

      // Refrescar la página
      await this.router.navigate(['/usuarios']);
    });
  }
}