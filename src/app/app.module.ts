// Módulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';

// Páginas
import { AppComponent } from './app.component';
import { LoginComponent } from './componentes/login/login.component';
import { DashboardComponent } from './componentes/dashboard/dashboard.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { UsuariosComponent } from './componentes/usuarios/usuarios.component';
import { DetalleUsuarioComponent } from './componentes/detalle-usuario/detalle-usuario.component';
import { MarcacionTarjetaComponent } from './componentes/marcacion-tarjeta/marcacion-tarjeta.component';
import { RegistroComponent } from './componentes/registro/registro.component';
import { AgregarTareasComponent } from './componentes/agregar-tareas/agregar-tareas.component';
import { EditarTareasComponent } from './componentes/editar-tareas/editar-tareas.component';

// Pipes
import { FiltrosPipe } from './pipes/filtros.pipe';
import { FechasPipe } from './pipes/fechas.pipe';

@NgModule({
  declarations: [
    AppComponent,
    UsuariosComponent,
    FiltrosPipe,
    DetalleUsuarioComponent,
    DashboardComponent,
    InicioComponent,
    MarcacionTarjetaComponent,
    RegistroComponent,
    AgregarTareasComponent,
    EditarTareasComponent,
    LoginComponent,
    FechasPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    ConfirmationPopoverModule.forRoot({
      confirmButtonType: 'danger' // set defaults here
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
