import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetalleUsuarioComponent } from './componentes/detalle-usuario/detalle-usuario.component';
import { UsuariosComponent } from './componentes/usuarios/usuarios.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { MarcacionTarjetaComponent } from './componentes/marcacion-tarjeta/marcacion-tarjeta.component';
import { RegistroComponent } from './componentes/registro/registro.component';
import { AgregarTareasComponent } from './componentes/agregar-tareas/agregar-tareas.component';
import { EditarTareasComponent } from './componentes/editar-tareas/editar-tareas.component';
import { LoginComponent } from './componentes/login/login.component';
import { GuardGuard } from './auth/guard.guard';
import { AuthguardGuard } from './auth/authguard.guard';

const routes: Routes = [
  {
    path: 'inicio',
    component: InicioComponent,
    canActivate: [AuthguardGuard]
  },
  {
    path: 'usuarios',
    component: UsuariosComponent,
    canActivate: [AuthguardGuard]
  },
  {
    path: 'detalle-usuario/:id/:name',
    component: DetalleUsuarioComponent,
    canActivate: [AuthguardGuard]
  },
  {
    path: 'marcacion-tarjeta',
    component: MarcacionTarjetaComponent,
    canActivate: [AuthguardGuard]
  },
  {
    path: 'registro',
    component: RegistroComponent,
    canActivate: [AuthguardGuard]
  },
  {
    path: 'agregar-tareas/:id/:name',
    component: AgregarTareasComponent,
    canActivate: [AuthguardGuard]
  },
  {
    path: 'editar-tareas/:id/:aid/:name/:aname/:acliente/:aproyecto/:adescription/:aavance/:aestado',
    component: EditarTareasComponent,
    canActivate: [AuthguardGuard]
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [GuardGuard]
  },
{
  path: '**',
  redirectTo: 'inicio'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
