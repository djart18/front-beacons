export interface Usuarios {
    id?: number;
    name?: string;
    email?: string;
}

export interface Activity {
    name?: string;
    estado?: string;
    description?: string;
    avance?: number;
    cliente?: string;
    proyecto?: string;
    d_name?: string;
    created_at?:string;
    
}
