export interface Activities {
    id?: number;
    name?: string;
    estado?: string;
    description?: string;
    avance?: number;
    cliente?: string;
    proyecto?: string;
    d_name?: string;
}
