export interface Checks {
    id?: string;
    clock?: string;
    check?: string;
    uid?: string;
    name?: string;
    beacon_uuid?: string;
    hora?: string;
    color?:string;
}