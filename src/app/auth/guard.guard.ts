import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {
  
  constructor() { }

  canActivate(): boolean {

  if (localStorage.getItem('currentUser')) {
    return false;

   } else {
    return true;
   }
   
  }
}
